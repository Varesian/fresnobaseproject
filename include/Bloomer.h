#pragma once

#include "cinder/Vector.h"
#include <vector>
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include "cinder/app/AppBasic.h"
#include "cinder/Color.h"
#include "cinder/Timeline.h"
#include "cinder/cairo/Cairo.h"
#include "cinder/gl/Texture.h"

using namespace ci;
using namespace ci::app;

class Bloomer {
public:
	Bloomer(Vec2f centroid);
	Bloomer();
	void update(float deltaTime);
	void draw();
private:
		void renderScene( cairo::Context &ctx );
			float colorOffset;
				float elapsedTime;
};