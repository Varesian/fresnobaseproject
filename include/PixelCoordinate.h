#pragma once

class Point {
    private:
        float x, y;
    public:
        Point() : x(0), y(0) {}
        Point(float x, float y) : x(x), y(y) {}
		void SetX(float val ) { x = val; }
		void SetY(float val ) { y = val; }
		float GetX() { return x; }
		float GetY() { return y; }
};