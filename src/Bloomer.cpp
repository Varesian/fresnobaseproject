#include "Bloomer.h"

Bloomer::Bloomer(Vec2f centroid) {
	colorOffset = 0;
	elapsedTime = 0;
}

Bloomer::Bloomer() {
	//do not use
}

void Bloomer::update(float deltaTime) {
	elapsedTime += deltaTime;
	colorOffset = sin(elapsedTime);
	colorOffset = lmap(colorOffset, -1.0f, 1.0f, 0.0f, 1.0f);
}

void Bloomer::draw() {
	cairo::SurfaceImage surface( 640, 480 );

//... draw using ctx ...

	cairo::Context ctx( surface );	
	renderScene( ctx );
	gl::Texture myTexture( surface.getSurface() );

	myTexture.enableAndBind();
	gl::color(ColorA(1.0f, 1.0f, 1.0f, 1.0f));
	gl::enableAdditiveBlending();
	ci::gl::drawSolidRect(Rectf(0, 0, getWindowSize().x, getWindowSize().y));
	myTexture.disable();
}

void Bloomer::renderScene( cairo::Context &ctx ) {
	float colorOffset2 = colorOffset + 0.5f;
	colorOffset2 = fmod(colorOffset2, 1.0f);

	// clear the context with our radial gradient
	cairo::GradientRadial radialGrad( getWindowCenter(), 0, getWindowCenter(), 100);
	radialGrad.addColorStop( 0, Color( colorOffset, 0, 0 ) );
	radialGrad.addColorStop( 1, Color( 0, colorOffset, colorOffset) );	
	radialGrad.addColorStop( 2, ColorA( 0, 0, 0, 0 ) );	
	ctx.setSource( radialGrad );	
	ctx.paint();
}
